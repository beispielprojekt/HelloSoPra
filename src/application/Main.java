package application;
	
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.HelloSoPraViewController;


/**
 * Main-Klasse des Projekts HelloSoPra.
 * @author Christian Riest
 *
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			HelloSoPraViewController sampleViewController = new HelloSoPraViewController();
			Scene scene = new Scene(sampleViewController,400,200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
