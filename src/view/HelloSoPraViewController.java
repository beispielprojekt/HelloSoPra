package view;

import java.io.IOException;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

public class HelloSoPraViewController extends BorderPane{
	
	public HelloSoPraViewController() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("HelloSoPraView.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		try {
			loader.load();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	

    @FXML
    void exit(ActionEvent event) {
    	Platform.exit();
    	System.exit(0);
    }
}
